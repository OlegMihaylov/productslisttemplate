**Catalog items list test app**

1. Added DetailsScreen for items
2. Store itemList cache, it is helpful to get item in details quickly. It is not perfect solution. In good way we must handle cases when there are no item in cache and load data from internet.
3. Handle no internet case. Now I don't show the error message, but in real application it must be.
