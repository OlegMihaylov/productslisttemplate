package com.mikhailov.testapp.network

import com.mikhailov.testapp.CatalogItem

data class GetListResponse(
    val products: List<CatalogItem>,
    val total: Int,
    val skip: Int,
    val limit: Int
)
