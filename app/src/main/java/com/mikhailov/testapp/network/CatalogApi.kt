package com.mikhailov.testapp.network

import retrofit2.http.GET

interface CatalogApi {

    @GET("products")
    suspend fun getList(): GetListResponse

}