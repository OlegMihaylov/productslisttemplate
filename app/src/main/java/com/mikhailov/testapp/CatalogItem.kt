package com.mikhailov.testapp

import java.math.BigDecimal

data class CatalogItem(
    val id: Long,
    val title: String,
    val description: String,
    val price: BigDecimal,
    val discountPercentage: BigDecimal,
    val thumbnail: String
)
