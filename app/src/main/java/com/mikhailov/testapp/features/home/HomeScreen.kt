package com.mikhailov.testapp.features.home

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.mikhailov.testapp.CatalogItem
import com.mikhailov.testapp.R
import java.math.BigDecimal

@Composable
fun HomeScreen(
    goToItemDetails: (itemId: Long) -> Unit,
) {
    val viewModel: HomeViewModel = hiltViewModel()
    val state = viewModel.state.collectAsState().value
    HomeScreen(state, goToItemDetails)
}

@Composable
private fun HomeScreen(
    catalogItems: List<CatalogItem>,
    goToItemDetails: (itemId: Long) -> Unit
) {
    Box(
        modifier = Modifier.background(Color(0XFFF2F2F2))
    ) {
        LazyColumn(
            contentPadding = PaddingValues(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            items(
                items = catalogItems,
                key = { it.id }
            ) { catalogItem ->
                CatalogItemView(catalogItem) {
                    goToItemDetails(catalogItem.id)
                }
            }
        }
    }
}

@Composable
private fun CatalogItemView(
    catalogItem: CatalogItem,
    clickListener: () -> Unit
) {
    Box(
        modifier = Modifier
            .clip(
                shape = RoundedCornerShape(8.dp)
            )
            .clickable { clickListener() }
            .background(Color.White)
    ) {
        Row(modifier = Modifier
            .padding(12.dp)
            .fillMaxWidth()
            .wrapContentHeight()
        ) {
            AsyncImage(
                modifier = Modifier
                    .size(72.dp),
                model = catalogItem.thumbnail,
                placeholder = painterResource(id = R.drawable.ic_product_placeholder),
                contentDescription = null,
            )
            Column(modifier = Modifier
                .weight(1f)
                .padding(start = 8.dp, end = 8.dp)
            ) {
                Text(
                    text = catalogItem.title,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
                Text(
                    text = catalogItem.description,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Normal
                    ),
                    color = Color(0XAA0f0f0f)
                )
            }
            Box(modifier = Modifier
                    .clip(
                        shape = RoundedCornerShape(4.dp)
                    )
                    .background(Color(0XFF333333)))
            {
                Text(
                    modifier = Modifier.padding(start = 4.dp, end = 4.dp),
                    text = catalogItem.price.toString(),
                    color = Color.White
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun HomeScreenPreview() {
    val itemList = listOf(
        CatalogItem(
            id = 1,
            title = "Coca cola",
            description = "description",
            price = BigDecimal.ONE,
            discountPercentage = BigDecimal.ZERO,
            thumbnail = ""
        ),
        CatalogItem(
            id = 1,
            title = "Pepsi",
            description = "description",
            price = BigDecimal.valueOf(125),
            discountPercentage = BigDecimal.ZERO,
            thumbnail = ""
        )
    )
    HomeScreen(itemList) { }
}



