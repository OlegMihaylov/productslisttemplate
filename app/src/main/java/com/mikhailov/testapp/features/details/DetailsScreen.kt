package com.mikhailov.testapp.features.details

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.mikhailov.testapp.CatalogItem
import com.mikhailov.testapp.R
import java.math.BigDecimal

@Composable
fun DetailsScreen(
    navigateUp: () -> Unit
) {
    val viewModel: DetailsViewModel = hiltViewModel()
    val state = viewModel.state.collectAsState().value.item ?: return
    DetailsScreen(item = state) { navigateUp() }
}

@Composable
private fun DetailsScreen(
    item: CatalogItem,
    navigateUp: () -> Unit
) {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier.matchParentSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(modifier = Modifier.fillMaxWidth()) {
                Image(
                    painter = painterResource(id = R.drawable.ic_back),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(16.dp)
                        .size(32.dp)
                        .clickable { navigateUp() }
                )
            }
            AsyncImage(
                modifier = Modifier
                    .padding(20.dp)
                    .size(140.dp),
                model = item.thumbnail,
                placeholder = painterResource(id = R.drawable.ic_product_placeholder),
                contentDescription = null,
                alignment = Alignment.Center
            )
            Row(modifier = Modifier
                .padding(12.dp)
                .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = item.title,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
                Box(modifier = Modifier
                    .clip(
                        shape = RoundedCornerShape(4.dp)
                    )
                    .background(Color(0XFF333333)))
                {
                    Text(
                        modifier = Modifier.padding(start = 4.dp, end = 4.dp),
                        text = item.price.toString(),
                        color = Color.White
                    )
                }
            }
            Text(
                text = item.description,
                style = TextStyle(
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Normal
                ),
                color = Color(0XAA0f0f0f),
                modifier = Modifier
                    .padding(12.dp)
                    .fillMaxWidth()
            )
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 400,
    heightDp = 600
)
@Composable
private fun DetailsScreenPreview() {
    val item = CatalogItem(
        id = 1,
        title = "Coca cola",
        description = "description",
        price = BigDecimal.ONE,
        discountPercentage = BigDecimal.ZERO,
        thumbnail = ""
    )
    DetailsScreen(item) { }
}
