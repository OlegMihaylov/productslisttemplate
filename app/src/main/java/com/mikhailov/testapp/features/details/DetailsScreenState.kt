package com.mikhailov.testapp.features.details

import com.mikhailov.testapp.CatalogItem

data class DetailsScreenState(
    val item: CatalogItem? = null
)
