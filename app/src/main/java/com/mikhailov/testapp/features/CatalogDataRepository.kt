package com.mikhailov.testapp.features

import com.mikhailov.testapp.CatalogItem
import com.mikhailov.testapp.network.CatalogApi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CatalogDataRepository @Inject constructor(
    private val catalogApi: CatalogApi
) {

    private var itemsCache: Map<Long, CatalogItem> = mutableMapOf()

    suspend fun getList(): List<CatalogItem> {
        val items = catalogApi.getList().products
        itemsCache = items.associateBy { it.id }
        return items
    }

    fun getItem(id: Long): CatalogItem? {
        return itemsCache[id]
    }

}