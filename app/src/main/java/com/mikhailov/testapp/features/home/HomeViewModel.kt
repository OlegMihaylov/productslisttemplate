package com.mikhailov.testapp.features.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mikhailov.testapp.CatalogItem
import com.mikhailov.testapp.features.CatalogDataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val catalogDataRepository: CatalogDataRepository
) : ViewModel() {

    private val _state = MutableStateFlow<List<CatalogItem>>(emptyList())
    val state = _state.asStateFlow()

    init {
        loadData()
    }

    private fun loadData() {
        viewModelScope.launch {
            try {
                val list = catalogDataRepository.getList()
                _state.value = list
            } catch (e: Exception) {
                // TODO show load catalog error
            }
        }
    }

}