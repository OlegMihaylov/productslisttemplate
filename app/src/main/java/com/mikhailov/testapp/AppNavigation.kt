package com.mikhailov.testapp

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.mikhailov.testapp.features.details.DetailsScreen
import com.mikhailov.testapp.features.home.HomeScreen

@Composable
fun AppNavigation(
    navController: NavHostController,
) {
    NavHost(
        navController = navController,
        startDestination = "home"
    ) {
        composable("home") {
            HomeScreen(
                goToItemDetails = { itemId ->
                    navController.navigate("item/$itemId")
                }
            )
        }
        composable(
            route = "item/{itemId}",
            arguments = listOf(
                navArgument("itemId") { type = NavType.LongType}
            )
        ) {
            DetailsScreen(
                navigateUp = {
                    navController.navigateUp()
                }
            )
        }
    }

}